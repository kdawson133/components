# Web-Components

A bower component which includes:

* bootstrap 3.3.5
* jquery 2.1.4
* font-awesome 4.4.0
* highlightjs 8.8.0

## Installation

    bower install https://bitbucket.org/kdawson133/components.git

Enjoy!
